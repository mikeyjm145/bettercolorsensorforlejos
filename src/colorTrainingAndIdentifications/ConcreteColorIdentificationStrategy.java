package colorTrainingAndIdentifications;

import java.util.ArrayList;
import java.util.HashMap;

import colorTrainingAndIdentifications.strategies.ColorIdentificationStrategy;
import io.DataFormatting;
import io.ioColorFileReader;
import model.Colors;
import model.LightLevel;
import view.EV3ScreenLog;

/**
 * Attempts to match a perceived color to a known color.
 * 
 * @author Michael Morguarge
 * @version 11/11/2016
 */
public class ConcreteColorIdentificationStrategy implements ColorIdentificationStrategy {

	private ArrayList<Colors> existingColors;
	private HashMap<Colors, double[]> scores;

	/**
	 * Creates the color identification training
	 * 
	 * @precondition colors cannot be null
	 * 
	 * @postcondition None
	 * 
	 * @param existingColors
	 *            The primary colors to identify
	 * @param calculatedEpsilon
	 *            The positive to negative range of error represented by a
	 *            positive value.
	 */
	public ConcreteColorIdentificationStrategy(ArrayList<Colors> existingColors) {
		if (existingColors == null) {
			throw new IllegalArgumentException("No colors exist yet.");
		}

		this.existingColors = existingColors;
		this.scores = new HashMap<Colors, double[]>();
	}

	/**
	 * Compares the perceived color to known colors and assigns a score.
	 * @precondition None.
	 * @param colorValues Perceived color to be scored
	 * @postcondition Perceived color should have been assigned a score
	 */
	@Override
	public void calculateConsiderationScores(float[] colorValues) {
		if (this.existingColors.isEmpty()) {
			EV3ScreenLog.add("No values read yet.");
			return;
		}

		// Compare with existing colors to find color name and the best shade of
		// color.

		for (Colors color : this.existingColors) {
			double[] shadeScores = new double[] { 
					color.compareColorTo(colorValues, LightLevel.BRIGHT),
					color.compareColorTo(colorValues, LightLevel.NORMAL),
					color.compareColorTo(colorValues, LightLevel.DARK) 
			};
			
			this.scores.put(color, shadeScores);
		}
	}

	/**
	 * Returns the name of a perceived color.
	 * @precondition None.
	 * @param None.
	 * @return Name of known color to which perceived color was matched
	 */
	@Override
	public String getBestMatch() {
		Colors bestMatchedColor = null;
		double lowestScore = Double.POSITIVE_INFINITY;
		String data = "";
		int bestIndex = 0;

		for (Colors color : this.scores.keySet()) {
			double[] colorScore = this.scores.get(color);
			data += color.getName() + " in ";
			for (int i = 0; i < colorScore.length; i++) {
				if (colorScore[i] < lowestScore) {
					lowestScore = colorScore[i];
					bestMatchedColor = color;
					bestIndex = i;
				}
				
				if (i < 2) {
					data += LightLevel.getConditionName(i % 3) + ":  " + colorScore[i] + ", ";
				} else {
					data += LightLevel.getConditionName(i % 3) + ":  " + colorScore[i] + DataFormatting.NewLine + DataFormatting.NewLine;
				}
			}
		}

		String best = bestMatchedColor + " in " + LightLevel.getConditionName(bestIndex % 3);
		data += "BEST MATCHED: " + best;

		ioColorFileReader.writeToFile(DataFormatting.ColorIDHeader, data, DataFormatting.SectionSeparator);

		return best;
	}
}
