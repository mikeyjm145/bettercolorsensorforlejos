/**
 * 
 */
package colorTrainingAndIdentifications.strategies;

/**
 * Defines color identification strategies
 * 
 * @author Michael Morguarge
 * @version 11/16/20116
 */
public interface ColorIdentificationStrategy {

	/**
	 * Calculates a consideration score. The closer to zero the better the
	 * option.
	 * 
	 * @precondition colorValues cannot be null.
	 * 
	 * @postcondition score is added to map of colors and scores.
	 * 
	 * @param colorValues The color values to compare with other color shades.
	 */
	void calculateConsiderationScores(float[] colorValues);
	
	/**
	 * Gets the best match based on the colors analyzed
	 * 
	 * @precondition None
	 * 
	 * @return the best matching color
	 */
	String getBestMatch();
}
