package model;

/**
 * Represents the light condition the color is in.
 * 
 * @author Michael Morguarge
 * @version 11/11/2016
 */
public enum LightLevel {
	NONE(-1), BRIGHT(0), NORMAL(1), DARK(2);

	public final int value;

	private LightLevel(int levelValue) {
		this.value = levelValue;
	}
	
	/**
	 * Gets the name of the light condition.
	 * 
	 * @precondition None
	 * 
	 * @param conditionNumber The numeric value of the light condition.
	 * 
	 * @return The name of the light condition.
	 */
	public static final String getConditionName(int conditionNumber) {
		String value = "no condition specified";
		
		switch (conditionNumber) {
			case 0:
				value = "Bright";
				break;
			case 1:
				value = "Normal";
				break;
			case 2:
				value = "Dark";
				break;
		}

		return value;

	}
}
