package controller;

import java.util.ArrayList;

import colorTrainingAndIdentifications.ConcreteColorIdentificationStrategy;
import colorTrainingAndIdentifications.strategies.ColorIdentificationStrategy;
import colorTrainingAndIdentifications.ConcreteColorTrainingStrategy;
import colorTrainingAndIdentifications.strategies.ColorTrainingStrategy;
import io.ioColorFileReader;
import lejos.hardware.port.SensorPort;
import model.ColorIdentifier;
import model.Colors;
import view.EV3ScreenLog;

/**
 * Controls the actions of the color sensor.
 * 
 * @author Michael Morguarge
 * @version 11/16/2016
 */
public class ColorSensorController {

	private ColorIdentificationStrategy identificationStrategy;
	private ColorTrainingStrategy trainingStrategy;
	private ColorIdentifier colorIdentifier;
	private float[] epsilon;

	/**
	 * Initializes the sensors, color training, and color identification.
	 * 
	 * @precondition None
	 * 
	 * @postcondition Sensors, color training, and color identification are
	 *                initialized.
	 */
	public ColorSensorController() {
		this.initSensors();
		this.initTrainingStrategy();
		this.initIdentificationStrategy();
	}

	private void initSensors() {
		this.colorIdentifier = new ColorIdentifier(SensorPort.S1, SensorPort.S2);
	}
	
	private void initTrainingStrategy() {
		this.trainingStrategy = new ConcreteColorTrainingStrategy(this.colorIdentifier);
		this.trainingStrategy.captureColor();
		this.epsilon = this.trainingStrategy.calculateEpsilon();

		EV3ScreenLog.add("RedE: " + this.epsilon[0]);
		EV3ScreenLog.add("GreenE: " + this.epsilon[1]);
		EV3ScreenLog.add("BlueE: " + this.epsilon[2]);

		EV3ScreenLog.add("Preparing Sensor...");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void initIdentificationStrategy() {
		ioColorFileReader.setFilePath("files/PrimaryColors.colors");
		ioColorFileReader.setEpsilon(this.epsilon);
		ArrayList<Colors> standardColors = ioColorFileReader.readColorFile();

		this.identificationStrategy = new ConcreteColorIdentificationStrategy(standardColors);
	}

	/**
	 * Starts reading colors from the color sensor when capture sensor is
	 * activated.
	 * 
	 * @precondition None
	 * 
	 * @postcondition The capture color is read and the best fit color is
	 *                printed out.
	 */
	public void startReading() {
		EV3ScreenLog.add("Scanning now");
		while (true) {
			boolean captureSensorTriggered = this.colorIdentifier.captureColorPressed();
			if (captureSensorTriggered) {
				this.colorIdentifier.getColor();
				float[] valuesRead = this.colorIdentifier.getColor();

				this.identificationStrategy.calculateConsiderationScores(valuesRead);
				EV3ScreenLog.add(this.identificationStrategy.getBestMatch());
			}

			this.delayExecutionForOneSecond();
		}
	}

	private void delayExecutionForOneSecond() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
