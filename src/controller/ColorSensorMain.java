/**
 * 
 */
package controller;

/**
 * Starts the program
 * 
 * @author Michael Morguarge
 * 
 * @version 11/09/2016
 */
public class ColorSensorMain {

	/**
	 * Starting point of the program.
	 * 
	 * @param args
	 *            Not used.
	 */
	public static void main(String[] args) {
		ColorSensorController controller = new ColorSensorController();
		controller.startReading();
	}
}
