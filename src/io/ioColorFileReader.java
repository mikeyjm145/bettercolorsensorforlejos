/**
 * 
 */
package io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import model.Colors;

/**
 * Reads the color file
 * 
 * @author Michael Morguarge
 * @version 11/16/2016
 */
public class ioColorFileReader {
	private static String filePath;
	private static float[] epsilonValues;

	/**
	 * Reads in a file of colors with the extension ".colors".
	 * 
	 * @precondition None
	 * 
	 * @postcondition The colors will be read in and created with their shades.
	 * 
	 * @return The list of created colors.
	 */
	public static ArrayList<Colors> readColorFile() {
		if (epsilonValues == null) {
			epsilonValues = new float[] { (float) 0.0, (float) 0.0, (float) 0.0 };
		}

		ArrayList<Colors> colors = new ArrayList<Colors>();
		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {

			String currentLine = "";

			while ((currentLine = br.readLine()) != null) {
				if (!currentLine.startsWith("%")) {
					String[] colorInfo = currentLine.split(";");
					String[] color = colorInfo[0].split(",");

					if ((colorInfo.length == 2) && (color.length == 3)) {

						float[] colorValues = new float[3];

						for (int i = 0; i < color.length; i++) {
							colorValues[i] = Float.valueOf(color[i]);
						}

						Colors existingColor = new Colors(colorInfo[1]);
						existingColor.addColorShade(colorValues, epsilonValues);

						colors.add(existingColor);
					} else {
						throw new IllegalArgumentException("The file is improperly formatted. Please try again.");
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return colors;
	}

	/**
	 * Writes data from program execution to a .colors file.
	 * 
	 * @precondition data cannot be null.
	 * 
	 * @postcondition The data is appended to the .colors file.
	 * 
	 * @param preFormatting
	 *            The formatting before the data is inserted.
	 * @param data
	 *            The information to store in the .colors file.
	 * @param postFormatting
	 *            The formatting after the data is inserted.
	 */
	public static void writeToFile(String preFormatting, String data, String postFormatting) {
		if (data == null) {
			throw new IllegalArgumentException("Data to write is null.");
		}

		String aPath = System.getProperty("user.dir").toString() + "/files/Results.colors";

		try (FileWriter fw = new FileWriter(aPath, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)) {
			if (preFormatting != null) {
				out.println(preFormatting);
			}
			out.println(data);
			if (postFormatting != null) {
				out.println(postFormatting);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets the file path to the ".color" file to be used.
	 * 
	 * @precondition fileName cannot be null
	 * 
	 * @postcondition File path is set.
	 * 
	 * @param fileName
	 */
	public static void setFilePath(final String fileName) {
		if (fileName == null || fileName.equals("") || !fileName.endsWith(".colors")) {
			throw new IllegalArgumentException("Path cannot be null or must have a '.colors' extension.");
		}

		filePath = System.getProperty("user.dir").toString() + "/" + fileName;
	}

	/**
	 * Sets the epsilon used for creating the colors.
	 * 
	 * @precondition epsilon cannot be null.
	 * 
	 * @postcondition epsilon values are set.
	 * 
	 * @param epsilon
	 *            The values used to create the color shades.
	 */
	public static void setEpsilon(final float[] epsilon) {
		if (epsilon == null) {
			throw new IllegalArgumentException("Epsilon cannot be null.");
		}

		epsilonValues = epsilon;
	}
}
