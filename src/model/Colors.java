/**
 * 
 */
package model;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * Creates a color with its values, name, and its light condition.
 * 
 * @author Michael Morguarge
 * @version 11/11/2016
 */
public class Colors {
	private static final float MAX_COLOR_VALUE = 255.0f;
	private static final float MAX_VALUE = 76.5f;
	private static final float MIN_VALUE = 0.0f;

	private final String colorName;
	private HashMap<LightLevel, float[]> colorShades;

	/**
	 * Creates the colors object. Cannot be modified after creation.
	 * 
	 * @param name
	 *            The name of the color.
	 */
	public Colors(final String name) {
		this.colorCreationCheck(name);

		this.colorName = name;
		this.colorShades = new HashMap<LightLevel, float[]>();
		float[] initData = new float[] { 0.0f, 0.0f, 0.0f };

		this.colorShades.put(LightLevel.BRIGHT, initData);
		this.colorShades.put(LightLevel.NORMAL, initData);
		this.colorShades.put(LightLevel.DARK, initData);
	}

	/**
	 * Copy constructor for Colors objects.
	 * 
	 * @precondition color cannot be null.
	 * 
	 * @postcondition color is copied into a new color object.
	 * 
	 * @param color
	 *            The color to copy.
	 */
	public Colors(final Colors color) {
		if (color == null) {
			throw new IllegalArgumentException("Color cannot be null.");
		}
		this.colorName = color.getName();
		this.colorShades = new HashMap<LightLevel, float[]>();

		for (Entry<LightLevel, float[]> entry : color.colorShades.entrySet()) {
			this.colorShades.put(entry.getKey(), entry.getValue());
		}
	}

	private void colorCreationCheck(String name) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Color name cannot be null.");
		}
	}

	/**
	 * Adds or overwrites a shade of the color to the specified light condition.
	 * 
	 * @precondition colorValues and light level cannot be null.
	 * 
	 * @postcondition The shade is inserted into the light condition slot.
	 * 
	 * @param colorValues
	 *            The values or shades of colors to add.
	 * @param lightingCondition
	 *            The condition of light.
	 */
	public void addColorShade(LightLevel lightingCondition, float[] colorValues) {
		if (colorValues == null) {
			throw new IllegalArgumentException("Cannot have null color shade values.");
		}

		if (lightingCondition == null) {
			throw new IllegalArgumentException("Lighting condition cannot be null.");
		}
		
		this.colorShades.put(lightingCondition, colorValues);
	}

	/**
	 * Adds a color shade to the color using the specified range to create light
	 * and dark color shades
	 * 
	 * @precondition colorValues and epsilon cannot be null.
	 * 
	 * @postcondition The color shade is added as normal shade and bright and
	 *                dark shades are created.
	 * 
	 * @param normalShade
	 *            The values or shades of color to add.
	 * @param epsilon
	 *            The colors darkness and brightness range.
	 */
	public void addColorShade(float[] normalShade, float[] epsilon) {
		if (normalShade == null) {
			throw new IllegalArgumentException("Color Values cannot be null.");
		}

		this.checkRawValues(normalShade);

		if (epsilon == null) {
			throw new IllegalArgumentException("Epsilon cannot be null.");
		}

		float[] brightShade = this.colorTransformation(normalShade, epsilon, LightLevel.BRIGHT);
		float[] darkShade = this.colorTransformation(normalShade, epsilon, LightLevel.DARK);

		this.colorShades.put(LightLevel.BRIGHT, brightShade);
		this.colorShades.put(LightLevel.NORMAL, normalShade);
		this.colorShades.put(LightLevel.DARK, darkShade);
	}

	private void checkRawValues(float[] values) {
		for (float value : values) {
			if (value < 0.0 || value > 1.0) {
				throw new IllegalArgumentException("Raw color values for this method only. (Values between 0 and 1.).");
			}
		}
	}

	private float[] colorTransformation(float[] colorValues, float[] epsilon, LightLevel condition) {
		float[] transformedColor = null;

		if (condition == LightLevel.BRIGHT) {
			float brightRed = this.adjustColor(colorValues[RGB.RED.value], epsilon[RGB.RED.value]);
			float brightGreen = this.adjustColor(colorValues[RGB.GREEN.value], epsilon[RGB.GREEN.value]);
			float brightBlue = this.adjustColor(colorValues[RGB.BLUE.value], epsilon[RGB.BLUE.value]);

			transformedColor = new float[] { brightRed, brightGreen, brightBlue };
		} else if (condition == LightLevel.DARK) {
			float darkRed = this.adjustColor(colorValues[RGB.RED.value], -epsilon[RGB.RED.value]);
			float darkGreen = this.adjustColor(colorValues[RGB.GREEN.value], -epsilon[RGB.GREEN.value]);
			float darkBlue = this.adjustColor(colorValues[RGB.BLUE.value], -epsilon[RGB.BLUE.value]);

			transformedColor = new float[] { darkRed, darkGreen, darkBlue };
		}

		return transformedColor;
	}

	private float adjustColor(float value, float epsilonValue) {
		float newValue = ((value * MAX_COLOR_VALUE) + epsilonValue);

		if (newValue < MIN_VALUE) {
			newValue = MIN_VALUE;
		} else if (newValue > MAX_VALUE) {
			newValue = MAX_VALUE;
		}

		return newValue / MAX_COLOR_VALUE;
	}

	/**
	 * The name of the color.
	 * 
	 * @return the name of the color.
	 */
	public final String getName() {
		return this.colorName;
	}

	/**
	 * The raw value of the color shade.Only used for color comparisons and
	 * computations.
	 * 
	 * @precondition lightCondition cannot be null.
	 * 
	 * @param lightingCondition
	 *            The light condition of the shade of color
	 * 
	 * @return The raw values for the color.
	 */
	public final float[] getRawColor(final LightLevel lightingCondition) {
		if (lightingCondition == null) {
			throw new IllegalArgumentException("Light condition cannot be null.");
		}

		return this.colorShades.get(lightingCondition);
	}

	/**
	 * The RGB value of the color shade. Only used for displaying color values.
	 * 
	 * @precondition lightCondition cannot be null.
	 * 
	 * @param lightingCondition
	 *            The light condition of the shade of color
	 * 
	 * @return The RGB values for the color.
	 */
	public final float[] getRGBColorShade(final LightLevel lightingCondition) {
		if (lightingCondition == null) {
			throw new IllegalArgumentException("Light condition cannot be null.");
		}

		return new float[] { this.colorShades.get(lightingCondition)[RGB.RED.value] * MAX_COLOR_VALUE,
				this.colorShades.get(lightingCondition)[RGB.GREEN.value] * MAX_COLOR_VALUE,
				this.colorShades.get(lightingCondition)[RGB.BLUE.value] * MAX_COLOR_VALUE };
	}

	/**
	 * Returns the color's name.
	 * 
	 * @return color name.
	 */
	@Override
	public final String toString() {
		return this.colorName;
	}

	/**
	 * Compares a color to color values read from color sensor.
	 * 
	 * @precondition color values cannot be null.
	 * 
	 * @postcondition None
	 * 
	 * @param colorValues
	 *            the color values read in by the color sensor.
	 * @param epsilon
	 *            The error range value
	 * @param lightingCondition
	 *            The lighting condition
	 * 
	 * @return The values of the differences combined.
	 */
	public final float compareColorTo(final float[] colorValues, final LightLevel lightingCondition) {
		if (colorValues == null) {
			throw new IllegalArgumentException("Invalid or null color values.");
		}
		
		float[] rgbColorShades = this.getRGBColorShade(lightingCondition);

		float redDifference = Math.abs(rgbColorShades[RGB.RED.value] - (colorValues[RGB.RED.value] * MAX_COLOR_VALUE));
		
		float greenDifference =  Math.abs(rgbColorShades[RGB.GREEN.value] - (colorValues[RGB.GREEN.value] * MAX_COLOR_VALUE));
		
		float blueDifference =  Math.abs(rgbColorShades[RGB.BLUE.value] - (colorValues[RGB.BLUE.value] * MAX_COLOR_VALUE));

		return (redDifference + greenDifference + blueDifference)/3.0f;
	}

	/**
	 * Compares two colors and returns each individual difference
	 * 
	 * @precondition color cannot be null
	 * 
	 * @postcondition None
	 * 
	 * @param color
	 *            The color to compare
	 * @param thatLightingCondition
	 *            The condition of light
	 * 
	 * @return The differences between each color
	 */
	public final float[] compareTo(Colors color, final LightLevel thisLightingCondition,
			final LightLevel thatLightingCondition) {
		if (color == null) {
			throw new IllegalArgumentException("Invalid or null color values.");
		}
		
		if (color == this) {
			return new float[] { 0.0f, 0.0f, 0.0f };
		} else {
			float[] thisRGBColorShades = this.getRGBColorShade(thisLightingCondition);
			float[] thatRGBColorShades = color.getRGBColorShade(thisLightingCondition);

			float redDifference = Math.abs(thisRGBColorShades[RGB.RED.value] - thatRGBColorShades[RGB.RED.value]);

			float greenDifference = Math.abs(thisRGBColorShades[RGB.GREEN.value] - thatRGBColorShades[RGB.GREEN.value]);

			float blueDifference = Math.abs(thisRGBColorShades[RGB.BLUE.value] - thatRGBColorShades[RGB.BLUE.value]);

			return new float[] { redDifference, greenDifference, blueDifference };
		}
	}
}
