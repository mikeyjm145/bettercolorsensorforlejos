package colorTrainingAndIdentifications;

import colorTrainingAndIdentifications.strategies.ColorTrainingStrategy;
import io.DataFormatting;
import io.ioColorFileReader;
import model.ColorIdentifier;
import model.Colors;
import model.LightLevel;
import model.RGB;
import view.EV3ScreenLog;

/**
 * The training for color identification.
 * 
 * @author Michael Morguarge
 * @version 11/16/2016
 */
public class ConcreteColorTrainingStrategy implements ColorTrainingStrategy {

	private static final int READINGS = 2;
	private static final String W_COLOR_NAME = "White";
	private static final String B_COLOR_NAME = "Black";
	private static final int RED = RGB.RED.value;
	private static final int GREEN = RGB.GREEN.value;
	private static final int BLUE = RGB.BLUE.value;
	private static final float coefficient = 11.0f;

	private ColorIdentifier colorSensorReading;
	private Colors white;
	private Colors black;

	/**
	 * Initializes the sensors needed for training.
	 * 
	 * @precondition colorSensorReading cannot be null.
	 * 
	 * @postcondition color sensors are ready for use in training.
	 * 
	 * @param colorSensorReading
	 *            The sensors to be used in color training.
	 */
	public ConcreteColorTrainingStrategy(ColorIdentifier colorSensorReading) {
		this.errorCheck(colorSensorReading);
		this.colorSensorReading = colorSensorReading;
		this.white = new Colors(W_COLOR_NAME);
		this.black = new Colors(B_COLOR_NAME);
	}

	private void errorCheck(ColorIdentifier colorSensorReading) {
		if (colorSensorReading == null) {
			new IllegalArgumentException("Sensors are not initialized.");
		}
	}

	/**
	 * Calibrates white and black for epsilon calculation.
	 * @precondition None.
	 * @param None.
	 * @postcondition User was able to calibrate black and white
	 */
	@Override
	public void captureColor() {
		EV3ScreenLog.add("Calibrating white.");

		int i = 0;
		int prevI = 0;
		Colors currColor = null;

		while (i < READINGS) {
			while (prevI == i) {
				boolean captureSensorTriggered = this.colorSensorReading.captureColorPressed();
				if (i == 0 && captureSensorTriggered) {
					EV3ScreenLog.add("Captured White");
					currColor = this.white;
					this.white.addColorShade(LightLevel.NORMAL, this.colorSensorReading.getColor());
					this.recordTrainingData(currColor, LightLevel.NORMAL);
					currColor = this.black;
					prevI++;

				} else if (i == 1 && captureSensorTriggered) {
					EV3ScreenLog.add("Captured Black");
					currColor = this.black;
					this.black.addColorShade(LightLevel.NORMAL, this.colorSensorReading.getColor());
					this.recordTrainingData(currColor, LightLevel.NORMAL);
					prevI++;
				}
			}

			if (i < READINGS - 1) {
				EV3ScreenLog.add("Capture " + currColor.getName());
			} else {
				float[] epsilon = this.calculateEpsilon();
				this.logEpsilon(epsilon);
				EV3ScreenLog.add("Reading Done.");
				this.recordTrainingData(null, LightLevel.NONE);
			}

			i++;
			this.slowDownExecution();
		}
	}

	private void recordTrainingData(Colors color, LightLevel level) {
		if (level == LightLevel.NONE) {
			ioColorFileReader.writeToFile(null, "Training Done!", DataFormatting.SectionSeparator);
		} else {
			float[] rgbColorShades = color.getRGBColorShade(level);
			String data = LightLevel.getConditionName(level.value) + " " + color.getName() + ": (" + rgbColorShades[RED] + ", "
					+ rgbColorShades[GREEN] + ", " + rgbColorShades[BLUE] + ")\r\n";
			ioColorFileReader.writeToFile(null, data, DataFormatting.NewLine);

			EV3ScreenLog.add(LightLevel.getConditionName(level.value) + " next.");
		}
	}

	private void slowDownExecution() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Calculates the epsilon value.
	 * @precondition None.
	 * @param None.
	 * @return majorColorDiff : A float array representing the epsilon value used for deriving brighter and darker shades
	 */
	@Override
	public float[] calculateEpsilon() {
		if (this.white == null) {
			throw new IllegalArgumentException("The calibration color does not exist yet.");
		}
		
		EV3ScreenLog.add(this.white.getRGBColorShade(LightLevel.NORMAL)[0] + "");
		EV3ScreenLog.add(this.white.getRGBColorShade(LightLevel.DARK)[0] + "");
		float[] majorColorDiff = this.white.compareTo(this.black, LightLevel.NORMAL, LightLevel.NORMAL);
		
		return new float[] { majorColorDiff[RED]/coefficient, majorColorDiff[GREEN]/coefficient, majorColorDiff[BLUE]/coefficient};
	}

	private void logEpsilon(float[] epsilon) {
		String data = "Epsilon: (" + epsilon[RED] + ", " + epsilon[GREEN] + ", " + epsilon[BLUE] + ")"
				+ DataFormatting.NewLine;
		ioColorFileReader.writeToFile(null, data, DataFormatting.NewLine);
	}
}
