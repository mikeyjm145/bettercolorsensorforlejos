package model;

import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.SampleProvider;

/**
 * Initializes connections between the color sensor and the lejos brick
 * 
 * @author Michael Morguarge
 * 
 * @version 11/09/2016
 */
public class ColorIdentifier {
	
	private EV3ColorSensor colorSensor;
	private EV3TouchSensor touchSensor;
	private static final int PRESSED = 1;

	/**
	 * Establishes the connection of the light sensor and lejos brick
	 * 
	 * @precondition lightPort cannot be null
	 * 
	 * @postcondition lightPort is used to establish connection to light sensor
	 * 
	 * @param lightPort
	 *            The port of the light sensor
	 */
	public ColorIdentifier(Port lightPort, Port captureColorButtonPort) {
		this.init(lightPort, captureColorButtonPort);
	}

	private void init(Port colorPort, Port captureColorButtonPort) {
		this.errorCheckSensors(colorPort, captureColorButtonPort);
		this.colorSensor = new EV3ColorSensor(colorPort);
		this.touchSensor = new EV3TouchSensor(captureColorButtonPort); 
	}

	private void errorCheckSensors(Port colorPort, Port captureColorButtonPort) {
		if (colorPort == null) {
			throw new IllegalArgumentException("Light Port cannot be null");
		}
		
		if (captureColorButtonPort == null) {
			throw new IllegalArgumentException("Button Port cannot be null");
		}
	}

	/**
	 * Returns the color values as an array of float values.
	 * 
	 * @precondition None.
	 * 
	 * @return Float values normalized between 0 and 1
	 */
	public float[] getColor() {
		SampleProvider color = this.colorSensor.getRGBMode();
		float[] sample = new float[color.sampleSize()];
		color.fetchSample(sample, 0);
		
		return sample;
	}
	
	/**
	 * Returns whether the button has been pressed.
	 * 
	 * @precondition None
	 * 
	 * @return whether the button was pressed.
	 */
	public boolean captureColorPressed() {
		SampleProvider buttonPressed = this.touchSensor.getTouchMode();
		float[] sample = new float[buttonPressed.sampleSize()];
		buttonPressed.fetchSample(sample, 0);
		
		return sample[0] == PRESSED;
	}

}
