package colorTrainingAndIdentifications.strategies;

/**
 * Defines color training strategies
 * 
 * @author Michael Morguarge
 * @version 11/16/20116
 */
public interface ColorTrainingStrategy {
	/**
	 * Uses the color reading at that moment to calibrate the values for epsilon
	 * 
	 * @precondition None
	 * 
	 * @postcondition None
	 */
	void captureColor();
	
	/**
	 * Calculates epsilon based on currently read values.
	 * 
	 * @precondition None
	 * 
	 * @postcondition None
	 * 
	 * @return The calculated epsilon value
	 */
	float[] calculateEpsilon();

}
