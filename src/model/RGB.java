package model;

/**
 * Represents RBG values.
 * 
 * @author Michael Morguarge
 * @version 11/11/2016
 */
public enum RGB {
	RED(0), GREEN(1), BLUE(2);

	public final int value;

	private RGB(int colorValue) {
		this.value = colorValue;
	}
}
