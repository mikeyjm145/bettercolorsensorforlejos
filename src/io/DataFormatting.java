/**
 * 
 */
package io;

/**
 * Set of global variables used for data output formatting.
 * 
 * @author Michael Morguarge
 * @version 11/18/2016
 */
public class DataFormatting {
	
	public static final String ColorIDHeader = "\r\n========================================================================\r\n"
			+ "Color Identification"
			+ "\r\n========================================================================";
	
	public static final String SectionSeparator = "\r\n========================================================================\r\n";
	
	public static final String NewLine = "\r\n";
}